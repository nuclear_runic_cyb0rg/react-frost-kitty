export {};

declare global {
  interface Window {
    IcecatLive?: {
      getDatasheet: (divs: any[], params: any, language: string) => Promise<void>;
    };
  }
  type FormDataProps = {
      icecatID: string,
      gtin: string,
      brand: string,
      mpn: string,
      username: string,
      type: string,
      language: string,
      granularData: string[]
      productStoryVersion: string
  };

  type BatchLinksArrayProps = {
    link: string,
    matchedBy: string
  }[]

  type TokensProps = {
    api_token: string,
    content_token: string,
  }

  type LocaleItem = {
    supportVertical: boolean,
    value: string,
    label: string,
    country?: string
  }
  type LocaleGroup = {
    group: string,
    items: LocaleItem[]
  }
  type RefFile = {
    info: string, 
    name: string, 
    schema: string, 
    isGzipped: boolean, 
    format: string,
    isUTF8?: boolean
    restricted?: boolean
  }
  //^ Special type for children
  type ProviderProps = {
  children: ReactNode
  }
}
  
declare module 'src/assets/img/*.png' {
    const value: string;
    export default value;
  }
  

