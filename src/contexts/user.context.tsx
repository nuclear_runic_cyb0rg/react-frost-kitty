import React, { createContext, useState, useContext } from "react"

const DEFAULT_USERNAME = "openIcecat-Live"

type UserContextType = {
    user: string,
    setUser: (user: string) => void
}

const defaultUserContext: UserContextType = {
    user: "",
    setUser: () => {}
}

export const UserContext = createContext<UserContextType>(defaultUserContext)

// RETURNS: {user, setUser}
export const UserProvider = ({ children } : ProviderProps) => {
  const [user, setUser] = useState(() => localStorage.getItem("username") || DEFAULT_USERNAME)
  return <UserContext.Provider value={{user, setUser}}>
    {children}
    </UserContext.Provider>
}

export const useUserContext = () => {
    return useContext(UserContext)
}