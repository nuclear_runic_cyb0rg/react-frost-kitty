import React from "react"
import { createContext, useState, useContext, useEffect } from "react"
import { fetchCollectionAsync } from "../db/fetchCollectionAsync.ts"

type LocalesContextType = {
    locales: LocaleGroup[],
    filterVerticals: (localesArray: LocaleGroup[]) => LocaleItem[]
} 

const defaultLocalesContext: LocalesContextType = {
    locales: [],
    filterVerticals: () => []
}

export const LocalesContext = createContext<LocalesContextType>(defaultLocalesContext)


const sortLocalesArray = (localesArray : LocaleGroup[]) : LocaleGroup[] => {
    for (let group of localesArray) {
      const getCountryName = (item: LocaleItem) => item.label.split(' ')[1]
      group.items.sort((a, b) => {
        const aLabel = getCountryName(a)
        const bLabel = getCountryName(b)
  
        if (aLabel < bLabel) return -1
  
        return 1
      })
    }
    return localesArray
  }


const filterVerticals = (localesArray : LocaleGroup[]) => {
        let langsArray: LocaleItem[] = []
        for (let group of localesArray) {
          group.items
            .filter(item => item.supportVertical)
            .forEach(item => {
              if (item.value === "EN") langsArray.unshift(item)
              else langsArray.push(item)
            })
        }
        return langsArray
    }


export const LocalesProvider = ({ children } : ProviderProps) => {
    const [locales, setLocales] = useState<LocaleGroup[]>([])
    useEffect(() => {
        const fetchLocales = async () => {
            const localesArray = await fetchCollectionAsync('locales')
            setLocales(sortLocalesArray(localesArray))
        }
        fetchLocales()
    }, [])

    return <LocalesContext.Provider value={{ locales, filterVerticals }}>
        {children}
    </LocalesContext.Provider>
}

export const useLocalesContext = () => {
    return useContext(LocalesContext)
}