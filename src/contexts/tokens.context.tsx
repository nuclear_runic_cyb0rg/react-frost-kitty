import React, { createContext, useState, useContext } from "react"

type TokensContextType = {
    tokens: TokensProps,
    setTokens: (tokens: any) => void
}



const defaultTokensContext: TokensContextType = {
    tokens: {
        api_token: '',
        content_token: '',
    },
    setTokens: () => {}
}


export const TokensContext = createContext<TokensContextType>(defaultTokensContext)

//* RETURNS: {tokens, setTokens}
//* tokens: {api_token, content_token}
export const TokensProvider = ({ children } : ProviderProps) => {
  const [tokens, setTokens] = useState({
    api_token: '',
    content_token: '',
  })
  return <TokensContext.Provider value={{tokens, setTokens}}>
    {children}
    </TokensContext.Provider>
}

export const useTokensContext = () => {
    return useContext(TokensContext)
}