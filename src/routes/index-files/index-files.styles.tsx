import styled from "styled-components"

export const IndexFilesStyles = styled.div`
    display: flex;
    flex-direction: column;
    margin: 10px 20%;
    background-color: #060622;
    padding: 1em;
    border-radius: 15px;
    gap: 10px;
`
export const InputsContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(3, minmax(0, 1fr));
    gap: 10px;
`
export const CheckboxContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: start;
    /* gap: 5px; */
`
export const RadioContainer = styled.div`
    /* background-color: rgb(0, 51, 54); */
    padding: 4px;
    padding-right: 10px;
    color: #f1e1ff;
    border-radius: 4px;
    display: flex;
    flex-direction: row;
    justify-content: center;
    gap: 10px;
    
`