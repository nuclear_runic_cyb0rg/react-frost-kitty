import {
  IndexFilesStyles,
  InputsContainer,
  CheckboxContainer,
  RadioContainer,
} from "./index-files.styles.tsx";
import {
  LanguageDropdown,
  VerticalsDropdown,
  FormCheckbox,
  ExportFile,
  RadioButton,
  FormInput,
  LoadingAnimation
} from "../../components";
import { constructIndexFileLink } from "../../helpers";
import { fetchCollectionAsync } from "../../db/fetchCollectionAsync";
import React, { ChangeEvent, MouseEvent, useEffect, useState } from "react";
import { useUserContext, useLocalesContext } from "../../contexts";

type IndexFile = {
  info: string;
  name: string;
  schema: string;
};

export const IndexFilesRoute = () => {
  const { setUser } = useUserContext();
  const getFileLink = (name: string) => {
    return constructIndexFileLink({
      ...indexFileParams,
      file: name,
    });
  };

  const [indexFileParams, setIndexFileParams] = useState({
    isFull: false,
    language: "EN",
    verticalID: "",
    isVertical: false,
    format: "xml",
  });

  const [indexFilesArray, setIndexFilesArray] = useState([]);
  const [prevLanguage, setPrevLanguage] = useState("");
  const [showLoading, setShowLoading] = useState(false);

  useEffect(() => {
    if (indexFilesArray.length > 0) return;
    setShowLoading(true);
    const fetchIndexFilesEffect = async () => {
      const indexFiles = await fetchCollectionAsync("indexFiles");
      setIndexFilesArray(indexFiles);
      setShowLoading(false);
    };
    fetchIndexFilesEffect();
  }, [indexFilesArray.length]);

  const { isVertical, language } = indexFileParams;
  // Only files.index.xml supports vertical requests.

  const filesIndex : IndexFile[] = indexFilesArray.filter((indexFile : IndexFile) => indexFile.name === "files.index")
  let visibleIndexFilesArray: IndexFile[] = isVertical
    ? filesIndex
    : indexFilesArray;

  const { locales, filterVerticals } = useLocalesContext();
  const verticalLanguagesArray = filterVerticals(locales);

  const handleUsernameChange = (event: MouseEvent) => {
    const { value } = event.target as HTMLInputElement;
    setUser(value);
  };

  const handleVerticalCheck = (event: ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target as HTMLInputElement;
    const activeLang = verticalLanguagesArray.find(
      (lang) => lang.value === language
    );
    const newLang = checked
      ? activeLang
        ? activeLang.value
        : "EN"
      : prevLanguage;
    if (checked) setPrevLanguage(language);

    setIndexFileParams({
      ...indexFileParams,
      format: "xml",
      isVertical: checked,
      language: newLang,
      verticalID: checked ? "107" : "",
    });
  };

  const handleFullCheck = (event: ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target as HTMLInputElement;
    setIndexFileParams({
      ...indexFileParams,
      isFull: checked,
    });
  };

  const handleLanguageChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const { value } = event.target as HTMLSelectElement;
    setIndexFileParams({
      ...indexFileParams,
      language: value,
    });
  };

  const handleVerticalChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const { value } = event.target as HTMLSelectElement;
    const newValue = isVertical ? value : "";
    setIndexFileParams({
      ...indexFileParams,
      verticalID: newValue,
    });
  };

  const handleFormatChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target as HTMLInputElement;
    setIndexFileParams({
      ...indexFileParams,
      format: value,
    });
  };
  return (
    <IndexFilesStyles>
      <InputsContainer>
        <FormInput
          onChange={handleUsernameChange}
          type="text"
          placeholder="Username"
          defaultValue={localStorage.getItem("username") || "openIcecat-Live"}
        />
        <LanguageDropdown
          handleLanguageChange={handleLanguageChange}
          includeINT={true}
          sortVertical={isVertical}
          activeValue={language}
        />
        {isVertical && <VerticalsDropdown onChange={handleVerticalChange} />}
      </InputsContainer>
      <CheckboxContainer>
        {!indexFileParams.isVertical && <RadioContainer>
          <RadioButton
            checked={indexFileParams.format === "xml"}
            name="format"
            value="xml"
            onChange={handleFormatChange}
          />
          <RadioButton
            checked={indexFileParams.format === "csv"}
            name="format"
            value="csv"
            onChange={handleFormatChange}
          />
        </RadioContainer>}
        <FormCheckbox
          checkboxName="isFull"
          labelValue={"I want Full Icecat access"}
          onCheck={handleFullCheck}
        />
        <FormCheckbox
          checkboxName="isVertical"
          labelValue={"I want a specific Vertical"}
          onCheck={handleVerticalCheck}
        />
      </CheckboxContainer>
      {showLoading && <LoadingAnimation />}
      {visibleIndexFilesArray.map((indexFile) => (
        <ExportFile
          info={indexFile.info}
          link={getFileLink(indexFile.name)}
          schema={indexFile.schema}
          key={indexFile.name}
          format={indexFileParams.format}
        />
      ))}
    </IndexFilesStyles>
  );
};
