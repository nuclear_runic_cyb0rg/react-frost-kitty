import React, { useState } from "react";
import {
  NavigationStyles,
  RootContainer,
  Background,
  MaskotImage,
  BetaTitle,
} from "./navigation.styles";
import { Outlet, useNavigate, useLocation } from "react-router-dom";
import { Button } from "../../components";

export const NavigationRoute = () => {
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const getActiveView = () => { 
    return pathname.split("/")[1] || "home"; 
  };
  const [activeView, setActiveView] = useState(getActiveView());
  // useEffect(() => window.scrollTo(0, 0), [activeView]);

  const changeView = (view: string) => {
    if (view === activeView) return;
    const isHome = view === "home";
    const route = isHome ? "/" : `/${view}`;
    setActiveView(view);
    navigate(route);
  };

  const handleNavClick = {
    home: () => changeView("home"),
    index: () => changeView("index"),
    refs: () => changeView("refs"),
    manuals: () => changeView("manuals"),
    helpers: () => changeView("helpers"),
  };

  const getTypeFor = (view: string) => {
    if (activeView === view) return "activeButtonShort";
    return "default";
  };

  return (
    <Background>
      <NavigationStyles>
        <MaskotImage src="maskot.png"></MaskotImage>
        <BetaTitle>Frost kitty BETA</BetaTitle>
        <Button
          onClick={handleNavClick.home}
          buttonType={getTypeFor("home")}
          text="Get Product"
        />
        <Button
          onClick={handleNavClick.index}
          buttonType={getTypeFor("index")}
          text="Index Files"
        />
        <Button
          onClick={handleNavClick.refs}
          buttonType={getTypeFor("refs")}
          text="Reference Files"
        />
        <Button
          onClick={handleNavClick.manuals}
          buttonType={getTypeFor("manuals")}
          text="Manuals"
        />
        <Button
          onClick={handleNavClick.helpers}
          buttonType={getTypeFor("helpers")}
          text="Helpers"
        />
      </NavigationStyles>
      <RootContainer>
        <Outlet />
      </RootContainer>
    </Background>
  );
};
