import styled, { keyframes } from "styled-components"
export const NavigationStyles = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: end;
  margin-right: 10px;
  gap: 5px;
  padding: 10px 0;
  position: sticky;
  top: 0px;
  background-color: #070727;
  opacity: 0.8;
  background-image:  linear-gradient(#000453 1px, transparent 1px), linear-gradient(to right, #000453 1px, #070727 1px);
  background-size: 25px 25px;
  z-index: 9;
  margin-bottom: 14px;
  @media (max-width: 1000px) {
    justify-content: center;
  }; 
`

export const RootContainer = styled.div`
  display: flex;
  flex-direction: column;
  color: white;
  min-height: 89.5vh;
  overflow-x: hidden;
  `

export const Background = styled.div`
    background: rgb(0, 0, 20);
    background: linear-gradient(90deg, #140002 0%, rgba(18, 32, 64, 1) 25%, #001430 50%, #000015 75%, #003742 100%);
    background-attachment: fixed;
    background-size: cover;
    `

export const MaskotImage = styled.img`
  position: absolute;
  top: 0;
  left: 2em;
  width: 65px;
  height: 65px;
  transform: translateY(0px);
  @media (max-width: 1000px) {
    left: 2px;
  }
`

const TripleColorfulJump = keyframes`
    0% {
        transform: translateY(-1px); /* Start at the original position */
    }
    15% {
        transform: translateY(-12px); /* First jump */
        color: #acecff
    }
    30% {
        transform: translateY(-1px); /* Back to the original position */
    }
    45% {
        transform: translateY(-12px); /* Second jump */
        color: #acffb1
    }
    60% {
        transform: translateY(-1px); /* Back to the original position */
    }
    75% {
        transform: translateY(-10px); /* Third jump */
        color: #ff85f9
    }
    100% {
        transform: translateY(-5px); /* Back to the original position */
    }
`;
export const BetaTitle = styled.div`
  position: absolute;
  z-index: -20;
  top: 0;
  left: 90px;
  font-size: 3em;
  padding: 10px 100px 10px 0px;
  color: white;
  font-family: 'ArcadeClassic', sans-serif;
  cursor: default;
  &:hover {
    animation: ${TripleColorfulJump} 1s ease-in 1;
    transform: translateY(-5px)
  }
  @media (max-width: 1000px) {
    display: none;
  }
  `