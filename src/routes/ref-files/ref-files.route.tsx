import { ExportFile, FormCheckbox, FormInput, LoadingAnimation } from "../../components";
import { constructRefFileLink } from "../../helpers";
import { RefFilesStyles, Settings } from "./ref-files.styles";
import React, { ChangeEvent, useEffect, useState } from "react";
// import { useUserContext } from "../../contexts";
import { fetchCollectionAsync } from "../../db/fetchCollectionAsync";
import { useDebouncedCallback } from "use-debounce";
// import { RefFile } from "../../../global.d";
export const RefFilesRoute = () => {
  const [isFull, setIsFull] = useState(false);
  // const { setUser } = useUserContext();
  const [refFilesArray, setRefFilesArray] = useState<RefFile[]>([]);
  const [filteredRefFiles, setFilteredRefFiles] = useState<RefFile[]>([]);
  const [displayedFiles, setDisplayedFiles] = useState<RefFile[]>([]);

  const [showLoading, setShowLoading] = useState(false);
  const [nothingFound, setNothingFound] = useState(false);

  // const [displayedFiles, setDisplayedFiles] = useState<RefFile[]>([]);



  useEffect(() => {
    if (refFilesArray.length > 0) return;
    setNothingFound(false);
    setShowLoading(true);
    const fetchRefFilesEffect = async () => {
      const refFiles : RefFile[] = await fetchCollectionAsync("refFiles");
      setRefFilesArray(refFiles.sort((a, b) => a.name.localeCompare(b.name)));
      setShowLoading(false);
    };
    fetchRefFilesEffect();
  }, [refFilesArray.length]);


useEffect(() => {
  let array = filteredRefFiles.length > 0 ? filteredRefFiles : refFilesArray;
  if (!isFull) {
    array = array.filter(file => !file.restricted);
  }
  setDisplayedFiles(array);
}, [isFull, filteredRefFiles, refFilesArray]);


  const handleFullCheck = (event: ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target as HTMLInputElement;
    setIsFull(checked);
  };

 
  const onSearchInput = async (event: ChangeEvent<HTMLInputElement>) => {
    setNothingFound(false);
    const { value } = event.target as HTMLInputElement;
    if (!value) {
      setFilteredRefFiles([]);
      return;
    }
    const filteredFiles = refFilesArray.filter((file) => file.schema.includes(value) || file.name.includes(value));
    if (filteredFiles.length === 0) {
      setNothingFound(true);
      return;
    }
    setFilteredRefFiles(filteredFiles.sort((a, b) => a.name.localeCompare(b.name)));
    setNothingFound(false);
  }

  const handleSearchChange = useDebouncedCallback(onSearchInput, 300)
  const returnExportFile = (file : RefFile) => {
    const { info, name, schema, isGzipped, format } = file;
    let isUTF = file.isUTF8 ? true : false;
    return (
      <ExportFile
        info={info}
        key={name+format+isUTF}
        schema={schema}
        link={constructRefFileLink(name, isFull, isGzipped, format, isUTF)}
        format={format}
      />
    )
  }

  return (
    <RefFilesStyles>
      <Settings>
        <FormInput
          onChange={handleSearchChange}
          type="text"
          placeholder="🔍 Search"
        />
        <FormCheckbox
          checkboxName="isFull"
          labelValue={"I want Full Icecat access"}
          onCheck={handleFullCheck}
        />
      </Settings>
      {showLoading && <LoadingAnimation />}
      
      {nothingFound ? <h2>Nothing found</h2>
      : displayedFiles.map(file => returnExportFile(file))
      }
      
    </RefFilesStyles>
  );
};
