import styled from "styled-components"

export const RefFilesStyles = styled.div`
    display: flex;
    flex-direction: column;
    gap: 1em;
    margin: 10px 20%;
    background-color: #060622;
    padding: 1em;
    border-radius: 15px;
    `
export const Settings = styled.div`
    display: grid;
    grid-template-columns: repeat(2, minmax(0, 1fr));
    gap: 10px;
`