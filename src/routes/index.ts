export { NavigationRoute } from "./navigation/navigation.route"
export { HomeRoute } from "./home/home.route"
export { IndexFilesRoute } from './index-files/index-files.route'
export { RefFilesRoute } from './ref-files/ref-files.route'
export { ManualsRoute } from './manuals/manuals.route.tsx'