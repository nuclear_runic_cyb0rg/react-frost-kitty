import styled from "styled-components";

export const ManualsStyles = styled.div`
    display: flex;
    flex-direction: column;
    margin: 10px 20%;
    background-color: #060622;
    padding: 1em;
    border-radius: 15px;
    gap: 10px;
`
    
export const ManualLink = styled.a`
    color: #d7d7ff;
    text-decoration: none;
    cursor: pointer;
    padding: 5px;
    border-radius: 5px;

    &:hover {
        color: #000000;
        background: rgb(183,179,255);
        background: linear-gradient(45deg, rgba(183,179,255,0.7355274873621324) 0%, rgba(240,168,255,1) 48%, rgba(225,67,236,1) 73%, rgba(0,212,255,1) 98%); 
        }
`