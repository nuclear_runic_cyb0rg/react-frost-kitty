import React, { useEffect, useState, useCallback } from "react";
import { fetchCollectionAsync } from "../../db/fetchCollectionAsync";
import {
  ManualsStyles,
  ManualLink,
} from "./manuals.styles.tsx";

import { 
    TooltipText, 
    LoadingAnimation 
} from "../../components/index.ts";

type ManualProps = {
  link: string;
  title: string;
  updated: boolean;
};

export const ManualsRoute = () => {
  const [manualsArray, setManualsArray] = useState([] as ManualProps[]);
  const [showLoading, setShowLoading] = useState(false);

  const fetchManualsEffect = useCallback(async () => {
    setShowLoading(true);
    const manuals = await fetchCollectionAsync("manuals");
    setManualsArray(manuals);
    setShowLoading(false);
  }, []);

  useEffect(() => {
    fetchManualsEffect();
  }, [fetchManualsEffect]);
  return (
    <ManualsStyles>
      {showLoading && <LoadingAnimation />}
      {manualsArray
      .sort((a, b) => a.title.localeCompare(b.title))
      .map((manual) => {
        const { link, title, updated } = manual as ManualProps;
        return (
            <ManualLink
              href={link}
              target="_blank"
              rel="noreferrer"
              key={title}
            >
             📖 {title} {updated && 
             <TooltipText text="✅" hint="Recently reviewed or updated" />
            }
            </ManualLink>
        );
      })}
    </ManualsStyles>
  );
};
