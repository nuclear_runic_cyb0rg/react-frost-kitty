import styled from "styled-components"

export const HomeStyles = styled.div`
    margin: 0px 22%;
    @media (max-width: 1000px) {
        margin: 0px 20px;
        
    }
`

export const FormData = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 10px;
    background-color: #060622;
    border-radius: 15px;
    padding-bottom: 10px;
`