import React, { ChangeEvent, MouseEvent, useState } from "react";
import { useUserContext } from "../../contexts";
import {
  getOutputForNewType,
  createBatchLinksArray,
  createNewGranularData,
} from "../../helpers";
import {
  TokensInput,
  LiveContainer,
  RequestTypeMenu,
  Credentials,
  ProductIdentifiers,
  ConditionalInputs,
  GranularMenu,
  LinksList,
} from "../../components";

import { useDebouncedCallback } from "use-debounce";
// Styles
import { FormData, HomeStyles } from "./home.styles";

export const HomeRoute = () => {
  const [formData, setFormData] = useState({
    type: "JSON",
    language: "EN",
    username: localStorage.getItem("username") || "openIcecat-Live",
    appKey: localStorage.getItem("appKey") || "",
    mpn: "",
    brand: "",
    gtin: "",
    icecatID: "",
    granularData: [""],
    output: "",
    isWideFeatures: false,
    relationsLimit: 0,
    productStoryVersion: ''
  });
  const [isBatchRequested, setIsBatchRequested] = useState(false);
  const [batchLinksArray, setBatchLinksArray] = useState<BatchLinksArrayProps>([]);
  const [language, setLanguage] = useState("EN");

  const { setUser } = useUserContext();
  const DEBOUNCE_DELAY = 300;

  const { type, gtin, icecatID } = formData;

  // ? Handlers
  const onInputChange = (event : ChangeEvent<HTMLInputElement>) => {
    let { dataset, value } = event.target as HTMLInputElement;
    const id : string = dataset.inputFor ? dataset.inputFor : "";
    if (id === '' ) return;
    value = value.trim();
    // console.log(value);
    // MPN allows uppercase only
    if (id === "mpn") value = value.toUpperCase();
    setFormData({
      ...formData,
      [id]: value,
    });
  };
  const handleInputChange = useDebouncedCallback(onInputChange, DEBOUNCE_DELAY);
  const handleUsernameChange = (event : ChangeEvent<HTMLInputElement>) => {
    let { value } = event.target as HTMLInputElement;
    value = value.trim();
    setUser(value);
    setFormData({
      ...formData,
      username: value,
    });
  };
  const handleLanguageChange = (event : ChangeEvent<HTMLSelectElement>) => {
    const { value } = event.target as HTMLSelectElement;
    setLanguage(value);
    setFormData({
      ...formData,
      language: value,
    });
  };
  const handleTypeButtonClick = (event : MouseEvent<HTMLButtonElement>) => {
    setBatchLinksArray([]);
    const {value} = event.target as HTMLInputElement;
    if (value === "Live") setIsBatchRequested(false);
    setFormData({
      ...formData,
      type: value,
      output: getOutputForNewType(value),
    });
  };
  const handleOutputDropdownChange = (event : ChangeEvent<HTMLSelectElement>) => {
    const {value} = event.target as HTMLSelectElement;
    setFormData({ ...formData, output: value });
  };
  const handleIsBatchRequested = (event : ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target as HTMLInputElement;
    setIsBatchRequested(checked);
  };
  const handleBatchRequest = () => {
    if (!isBatchRequested) return;
    setBatchLinksArray(createBatchLinksArray(gtin, icecatID, formData));
  };
  const handleActivateCheckbox = (event : ChangeEvent<HTMLInputElement>) => {
    const { checked, value } = event.target as HTMLInputElement;
    setFormData({
      ...formData,
      granularData: createNewGranularData(checked, value, formData),
    });
  };
  const handleIsWideFeatures = (event : ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target as HTMLInputElement;
    const isChecked = checked ? true : false;
    setFormData({
      ...formData,
      isWideFeatures: isChecked,
    });
  };

  const handlePSVersionChange = (event : ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target as HTMLInputElement;
    const isChecked = checked ? "v2" : "";
    setFormData({
      ...formData,
      productStoryVersion: isChecked
    })
  }

  const conditionalInputHandlers = {
    handleOutputDropdownChange,
    handleLanguageChange,
    handleIsBatchRequested,
    handleIsWideFeatures,
    handleInputChange,
    handlePSVersionChange
  };

  return (
    <HomeStyles>
      <FormData>
        <RequestTypeMenu
          type={type}
          handleTypeButtonClick={handleTypeButtonClick}
        />
        <Credentials
          type={type}
          handleUsernameChange={handleUsernameChange}
          handleInputChange={handleInputChange}
        />
        <ProductIdentifiers
          type={type}
          isBatchRequested={isBatchRequested}
          handleInputChange={handleInputChange}
        />
        <ConditionalInputs
          type={type}
          conditionalInputHandlers={conditionalInputHandlers}
          activeLang={language}
        />
        <GranularMenu
          type={type}
          handleActivateCheckbox={handleActivateCheckbox}
        />
        <TokensInput type={type} />
        <LiveContainer formData={formData} />
        <LinksList
          formData={formData}
          isBatchRequested={isBatchRequested}
          handleBatchRequest={handleBatchRequest}
          batchLinksArray={batchLinksArray}
        />
      </FormData>
    </HomeStyles>
  );
};
