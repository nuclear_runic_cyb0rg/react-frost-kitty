// const  fs = require('fs')

const data = document.querySelector('#table-with-many-data')

console.log(data);

let output = []
const headers = [] 

for (let header of data.querySelectorAll('tr th')) headers.push(header.innerText)
console.log(headers);

for (let row of data.querySelectorAll('tr')) {
    let rowData = row.querySelectorAll('td')
    let i = 0
    let rowObj = {}
    for (let cell of rowData) {
        rowObj[headers[i]] = cell.innerText
        i++
    }
    output.push(rowObj)
}

const jsonOutput = JSON.stringify(output, null, 2)
// Create a Blob with the JSON data
const blob = new Blob([jsonOutput], {type: 'application/json'});

// Create a URL for the Blob
const url = URL.createObjectURL(blob);

// Create a temporary anchor element
const a = document.createElement('a');
a.href = url;
a.download = 'output.json'; // Name of the file to be downloaded

// Programmatically click the anchor to trigger the download
a.click();

// Clean up by revoking the Object URL
URL.revokeObjectURL(url);