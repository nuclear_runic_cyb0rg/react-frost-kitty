// Styled components will be here
import "./App.css";
// System imports
import React from "react";
import { Routes, Route } from "react-router-dom";

import {
  HomeRoute,
  NavigationRoute,
  IndexFilesRoute,
  RefFilesRoute,
  ManualsRoute,
} from "./routes/index.ts";

// Contexts
import { 
  UserProvider, 
  TokensProvider, 
  LocalesProvider 
} from "./contexts/index.ts";

function App() {
  return (
    <UserProvider>
      <TokensProvider>
        <LocalesProvider>
          <Routes>
            <Route path="/" element={<NavigationRoute />}>
              <Route index element={<HomeRoute />}></Route>
              <Route path="index" element={<IndexFilesRoute />}></Route>
              <Route path="refs" element={<RefFilesRoute />}></Route>
              <Route path="manuals" element={<ManualsRoute />}></Route>
              <Route path="helpers" element={<h1>Working on it!</h1>}></Route>
            </Route>
          </Routes>
        </LocalesProvider>
      </TokensProvider>
    </UserProvider>
  );
}
//& Summary
// Home router: menu with the API interface to access the data
// Navigation: links to: Home, Index Files, Reference files, Manuals

export default App;
