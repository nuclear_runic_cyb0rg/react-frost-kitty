import React, { ChangeEvent } from 'react'
import { FormCheckboxStyles, FormCheckboxLabelStyles } from './formCheckbox.styles'

type FormCheckboxProps = {
    checkboxName: string,
    labelValue: string,
    onCheck: (event : ChangeEvent<HTMLInputElement>) => void,
    id?: string
}

export const FormCheckbox = ({ checkboxName, labelValue, onCheck, id = "" } : FormCheckboxProps) => {

    return (
        <FormCheckboxLabelStyles htmlFor={id || checkboxName}>
            <FormCheckboxStyles type="checkbox"
                id={id || checkboxName}
                name={checkboxName}
                value={checkboxName}
                onChange={onCheck} />
            {labelValue}
        </FormCheckboxLabelStyles>
    )
}