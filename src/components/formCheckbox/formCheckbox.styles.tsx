import styled, { keyframes} from 'styled-components';


export const FormCheckboxStyles = styled.input`
    width: 20px;
    height: 20px;
    position: relative;
    background-color: #1c1f26; /* Dark charcoal background */
    border: 2px solid #4a5d7d; /* Subtle border color */
    border-radius: 4px; /* Slightly rounded for a playful touch */
    transition: background-color 0.3s ease, border-color 0.3s ease;
    appearance: none;
    
    &:hover {
        border: 2px solid #488bff; /* Subtle border color */
    }
    
    &:checked::before {
        content: "";
        position: absolute;
        top: 0px;
        left: 4px;
        width: 6px;
        height: 12px;
        border: solid #fff; /* White checkmark color */
        border-width: 0 4px 4px 0;
        transform: rotate(45deg);
    }
    `   


const changeTextColor = keyframes`
    0% {
        color: #007afc;
    }
    50% {
        color: #ffffff;
    }
    100% {
        color: #6db1ff;
    }
`
export const FormCheckboxLabelStyles = styled.label`
    display: flex;
    align-items: center;
    color: #8af5f6;
    padding-left: 15px;
    
    &:hover {
        text-shadow: 0 0 10px #4a92ff;
        cursor: pointer;
    }
    
    &:active {
        animation: ${changeTextColor} 0.1s ease-out forwards;
    }
    `