import styled from "styled-components"

export const ExportFileContainer = styled.div`
    display: flex;
    flex-direction: column;
    gap: 5px;  
    background-color: #1a1a2e;
    color: #cef9ff;
    padding: 20px;
    border-radius: 10px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
    transition: 
        transform 0.3 ease, 
        flex 1 ease;

    &:hover {
        transform: translateY(-5px);
        box-shadow: 0 8px 16px rgba(0, 0, 0, 0.3);
        background-color: #16213e;
        border: 1px solid #00aaff;
    }
`;

export const Title = styled.div`
    justify-content: space-between;
    align-items: center;
    display: flex;
    flex-direction: row;
`

export const FileLink = styled.a`
    color: #69cdff;
    text-decoration: none;
    font-weight: bold;
    font-size: 1.2em;
    margin-bottom: 12px;
    margin-left: 5px;
    transition: color 0.3s ease, text-shadow 0.3s ease;

    &:hover {
        color: #66d9ff;
        text-shadow: 0 0 5px #00aaff;
    }

`;

export const ExportFileButtons = styled.div`
    display: flex;
    gap: 10px;
    padding-bottom: 10px;
`;

export const ImageButton = styled.img`
    background-color: #282c34;
    color: #ffffff;
    border: 2px solid #4e5b6e;
    padding: 8px 16px;
    max-width: 20px;
    max-height: 20px;
    border-radius: 8px;
    cursor: pointer;
    font-size: 0.9em;
    transition: background-color 0.3s ease, border-color 0.3s ease, box-shadow 0.3s ease;

    &:hover {
        background-color: #364158;
        border-color: #00aaff;
        box-shadow: 0 0 8px #00aaff;
    }

    &:active {
        background-color: #01790b;
        border-color: #66d9ff;
    }
`;


export const TextContainer = styled.div`
    color: #94fdff;
    padding: 14px;
`   
export const SchemaContainer = styled.div`
    overflow-x: auto;
    overflow-y: auto;
    font-size: 0.7em;
`

export const Panel = styled.div`
    display: flex;
    justify-content: space-between;
    gap: 10px;
    flex-direction: row;
    width: 100%;
    `