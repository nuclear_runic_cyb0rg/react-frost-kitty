import React, { useState } from "react";

//images
import { copyButton, infoButton, viewGetButton, done } from "../../assets/img";

import {
  ExportFileContainer,
  ExportFileButtons,
  FileLink,
  ImageButton,
  Title,
  TextContainer,
  Panel,
} from "./exportFileContainer.styles";
import { CodeHighlighter, WgetContainer } from "../";

type ExportFileProps = {
  info: string;
  link: string;
  schema?: string;
  format: string;
};

export const ExportFile = ({
  info,
  link,
  schema = "",
  format,
}: ExportFileProps) => {
  const [linkCopied, setLinkCopied] = useState(false);
  const [show, setShow] = useState({
    showInfo: false,
    showwGet: false,
  });
  const { showInfo, showwGet } = show;
  const handleInfoView = () => {
    setShow({
      ...show,
      showInfo: !showInfo,
    });
  };

  const handlewGetView = () => {
    setShow({
      ...show,
      showwGet: !showwGet,
    });
  };

  const handleCopyLink = () => {
    navigator.clipboard.writeText(link);
    setLinkCopied(true);
    setTimeout(() => {
      setLinkCopied(false);
    }, 1000);
  };
  const pathArray = link.split("/");
  const title = pathArray[pathArray.length - 1];
  const getImage = (format: string) => {
    return `${format}_icon.png`;
  };
  return (
    <ExportFileContainer>
      <Panel>
        <Title>
          <img src={getImage(format)} alt={format} />
          <FileLink href={link} target="_blank" rel="noreferrer">
            {title}
          </FileLink>
        </Title>

        <ExportFileButtons>
          <ImageButton src={infoButton} onClick={handleInfoView} alt="info" />
          <ImageButton
            src={viewGetButton}
            onClick={handlewGetView}
            alt="view"
          />
          <ImageButton
            src={linkCopied ? done : copyButton}
            onClick={handleCopyLink}
            alt={linkCopied ? "done" : "copy"}
          />
        </ExportFileButtons>
      </Panel>
      {showInfo && (
        <>
          <TextContainer>{"🛈 " + info}</TextContainer>
          {schema && <CodeHighlighter content={schema} language="xml" />}
        </>
      )}
      {showwGet && <WgetContainer link={link} key={title + format} />}
    </ExportFileContainer>
  );
};
