import { Column } from "../common/column.styles.tsx"
import { Row } from "../common/row.styles.tsx"
import { FormInput } from "../formInput/formInput.component.tsx"
import React, { ChangeEvent, useState } from "react"

type CredentialsProps = {
  type: string;
  handleUsernameChange: (event: ChangeEvent<HTMLInputElement>) => void;
  handleInputChange: (event: ChangeEvent<HTMLInputElement>) => void;
};

export const Credentials = ({ type, handleUsernameChange, handleInputChange } : CredentialsProps) => {
  const DEFAULT_USERNAME = "openIcecat-Live"
  const [credentials, setCredentials] = useState({
    username: localStorage.getItem("username") || DEFAULT_USERNAME,
    appKey: localStorage.getItem("appKey") || ""
  });


  const onFieldChange = (field : string) => (event : ChangeEvent<HTMLInputElement>) => {
    let { value } = event.target as HTMLInputElement
    value = value.trim()
    localStorage.setItem(field, value)
    setCredentials({
      ...credentials,
      [field]: value
    })
    if (field === "username") handleUsernameChange(event)
    if (field === "appKey") handleInputChange(event)
    return 
  }
  const onUsernameChange = onFieldChange("username")
  const onAppKeyChange = onFieldChange("appKey")
  return (
    <Column>
      <Row>
        <FormInput
          data-input-for="username"
          type="text"
          onChange={onUsernameChange}
          placeholder="Username"
          value={credentials.username}
          />
          
        {type === "JSON" && <FormInput
          data-input-for="appKey"
          type="text"
          onChange={onAppKeyChange}
          placeholder="App Key" 
          value={credentials.appKey}
          />
        }
      </Row>
    </Column>
  )
}