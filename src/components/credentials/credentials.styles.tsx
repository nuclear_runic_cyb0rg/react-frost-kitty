import styled from "styled-components"

export const FormFieldset = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    gap: 5px;
    border: 0px;
    padding: 5px 11px;
`
export const FormFieldsetRow = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    gap: 10px;
    width: 100%;
`