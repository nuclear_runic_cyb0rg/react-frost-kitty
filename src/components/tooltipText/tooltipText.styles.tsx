import styled from "styled-components"

export const Hint = styled.div`
  position: absolute;
  visibility: hidden;
  opacity: 0;
  top: 0;
  left: 2em;
  /* width: 100%; */
  /* height: 100%; */
  background-color: rgba(21, 0, 51, 0.937);
  min-width: 120px;
  color: white;
  font-size: 0.8rem;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0.5rem;
 `
export const TooltipTextStyles = styled.span`
  position: relative;

  &:hover ${Hint} {
    visibility: visible;
    opacity: 1;
  }
  `
