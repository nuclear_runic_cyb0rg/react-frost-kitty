import React from "react"
import { TooltipTextStyles, Hint } from "./tooltipText.styles.tsx"

type TooltipTextProps = {
    text: string,
    hint: string
}

export const TooltipText = ({ text, hint } : TooltipTextProps) => {
    return (
    <TooltipTextStyles>
        {text}
        <Hint>{hint}</Hint>
    </TooltipTextStyles>)
}