import styled from 'styled-components';

export const ManualLinkStyles = styled.div`
    position: relative;
    display: block;
    background-color: #000f28;
    color: #ffffffe7;
`