import React from 'react';

type ManualLinkProps = {
    manualItem: {
        title: string,
        link: string
    }
}

// const ManualLink = ({ manualItem } : ManualLinkProps) => {
//     const { title, link } = manualItem;
//     return (
//         <ManualLinkStyles>
//             <a href={link} target="_blank" rel="noopener noreferrer">{title}</a>
//         </ManualLinkStyles>
//     )
// }