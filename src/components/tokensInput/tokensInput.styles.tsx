import styled from "styled-components"

export const TokensInputContainer = styled.div`
    display: flex;
    flex-direction: column;
    gap: 5px;
    border: 0px;
    padding: 15px;
`