import React from "react"
import { useTokensContext } from "../../contexts/tokens.context.tsx"
import { FormInput } from "../formInput/formInput.component.tsx"
import { TooltipText } from "../tooltipText/tooltipText.component.tsx"
import { TokensInputContainer } from "./tokensInput.styles.tsx"

type TokensInputProps = {
  type: string
}
export const TokensInput = ({type} : TokensInputProps) => {
  const { tokens, setTokens } = useTokensContext()
  if (type !== "FO" && type !== "Live") {

    const hintTxt = "If entered, will render a wGet with tokens passed as headers."

    const handleTokenInputChange = (event : MouseEvent) => {
        const { value, dataset } = event.target as HTMLInputElement
        const id = dataset.inputFor ? dataset.inputFor.toString() : ""
        if (!id) return
        setTokens({
          ...tokens,
          [id]: value
        })
      }
    
    return (
      <TokensInputContainer>
        <div>Access tokens <TooltipText text='❓' hint={hintTxt} /></div>
        <FormInput
          data-input-for="api_token" type="text"
          onChange={handleTokenInputChange}
          placeholder="API token" 
          value={tokens.api_token || ""}
          />
        <FormInput
          data-input-for="content_token" type="text"
          onChange={handleTokenInputChange}
          placeholder="Content token"
          value={tokens.content_token || ""} 
          />
      </TokensInputContainer>)
  }
}