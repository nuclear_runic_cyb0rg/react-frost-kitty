import styled from "styled-components"

export const Column = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    gap: 5px;
    border: 0px;
    padding: 5px 11px;
`