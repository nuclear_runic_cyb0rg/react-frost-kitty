import React, { MouseEvent } from "react"
import { RequestTypeMenuStyles } from "./requestTypeMenu.styles.tsx"
import { Button } from "../button/button.component.tsx"

const typesArray = ['XML',"JSON","CSV","FO","Live"]

type RequestTypeMenuProps = {
    type: string,
    handleTypeButtonClick: (event: MouseEvent<HTMLButtonElement>) => void
}

export const RequestTypeMenu = ({ type, handleTypeButtonClick } : RequestTypeMenuProps) => {
    return (
      <RequestTypeMenuStyles>
        {typesArray.map(item => {
          return (<Button
            buttonType={type === item ? "activeButton" : "typeButton"}
            onClick={handleTypeButtonClick}
            key={item}
            value={item}
            text={item} />)
        })}
      </RequestTypeMenuStyles>)
  }