import styled from "styled-components"

export const RequestTypeMenuStyles = styled.div`
    margin-bottom: 15px;
    padding: 1em;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: stretch;
`