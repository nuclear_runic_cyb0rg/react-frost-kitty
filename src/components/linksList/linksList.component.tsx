import React from "react"
import { constructFOLink, constructProductLink } from "../../helpers/index.ts";
import { Button, WgetContainer, ShowLink } from "../index.ts";
import { Column } from "../common/column.styles.tsx";

import { useTokensContext } from "../../contexts/tokens.context.tsx"


type LinksListProps = {
    formData: FormDataProps,
    isBatchRequested: boolean,
    handleBatchRequest: () => void,
    batchLinksArray: BatchLinksArrayProps
}

const getLinkLabel = (formData : FormDataProps) => {
    return { 
      key: getTheHighestPriorityIdentifierKey(formData), 
      value: getTheHighestPriorityIdentifierValue(formData)
    };
  };

const getTheHighestPriorityIdentifierValue = (formData : FormDataProps) => {
  const { icecatID, gtin, brand, mpn } = formData
  return icecatID || gtin || (brand && mpn ? `${mpn}/${brand}` : null);
}

const getTheHighestPriorityIdentifierKey = (formData : FormDataProps) => {
  const { icecatID, gtin, brand, mpn } = formData
  return icecatID ? "IcecatID" : gtin ? "GTIN" : brand && mpn ? "MPN/Brand" : "";
}

const returnBatch = {
    
    links: (array : BatchLinksArrayProps, type: string) => {
      return array.map(item =>
        <ShowLink requestType={type} link={item.link}
          matchedBy={item.matchedBy} key={item.matchedBy} />)
    },
    wgets: (array : BatchLinksArrayProps) => {
      return array.map(item =>
        <WgetContainer link={item.link} key={item.matchedBy} />)
    }
  }

export const LinksList = ({formData, isBatchRequested, handleBatchRequest, batchLinksArray} : LinksListProps) => {
    const { tokens } = useTokensContext()
    const isAnyTokenEntered = Object.values(tokens).some(value => typeof value === 'string' && value.trim() !== '');
    console.log(tokens, isAnyTokenEntered)
    
    const { type, icecatID } = formData
    const { key, value } = getLinkLabel(formData)
    const isLive = type === "Live"
    const isFO = type === "FO"
    const link = isFO ? constructFOLink(icecatID) : constructProductLink(formData)
  
    const showLinksCondition = !isAnyTokenEntered && isBatchRequested

    const showWgetsCondition = isAnyTokenEntered && isBatchRequested
    const showSingleLinkCondition = !isAnyTokenEntered && !isBatchRequested && !(!icecatID && isFO)
    const showSingleWgetCondition = isAnyTokenEntered && !isBatchRequested
    
    if (!value || isLive) return null
    return (
      <Column>
        {isBatchRequested &&
          <><Button text="Show links" onClick={handleBatchRequest} />
            {showLinksCondition && returnBatch.links(batchLinksArray, type)}
            {showWgetsCondition && returnBatch.wgets(batchLinksArray)}</>}
  
        {showSingleLinkCondition && <ShowLink requestType={type} link={link} matchedBy={`${key} ${value}`} key={key} />}
        {showSingleWgetCondition && <WgetContainer link={link} key='wget' />}
  
      </Column>)
  }