type ObjectProps = {
  'UserName': string,
  'IcecatProductId'?: string,
  'GTIN'?: string,
  'Brand'?: string,
  'ProductCode'?: string,
  'productStoryVersion'?: string
}

export const getParams = (formData : FormDataProps) => {
    const {
      username,
      icecatID,
      gtin,
      brand,
      mpn,
      productStoryVersion
    } = formData
  
    let obj : ObjectProps = { 
      'UserName': username,
    }
    if (productStoryVersion === 'v2') {
      obj['productStoryVersion'] = 'v2'
    }
    if (icecatID) {
      obj['IcecatProductId'] = icecatID
    }
    if (gtin) {
      obj['GTIN'] = gtin

    }
    if (brand && mpn) {
      obj['Brand'] = brand
      obj['ProductCode'] = mpn
    }
    return obj
  }