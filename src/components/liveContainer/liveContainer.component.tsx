import React from "react"
import {
  LiveContainerAbsoluteStyles,
  LiveContainerRelativeContainer,
  CloseButton,
  Error,
  LiveOutput,
  GranularOutput,
  StickyHeader,
  ErrorImage,
  ErrorMessage
} from "./liveContainer.styles"
import { getLinkLabel } from "../../helpers/getLinkLabel";
import { Button } from "../"
import { useState, useEffect } from "react";

import { getParams } from "./getParams";
import { getDivs } from "./getDivs";

const ICECAT_LIVE_URL = 'https://live.icecat.biz/js/live-current-2.js'
const getButtonText = (showMagic : boolean) => showMagic ? "🪄✨" : "Show product Live HTML"

type ErrorProps = {
  error: string
}

const ErrorContainer = ({ error } : ErrorProps) => {
  return (
    <>
      <ErrorImage src='/error.png' alt='error'></ErrorImage>
      <Error>Error:&nbsp;<ErrorMessage>{error}</ErrorMessage></Error>
      <Error>Please check your inputs and try again.</Error>
    </>
  )
}

type OutputContainerProps = {
  granularData: string[]
}

const OutputContainer = ({ granularData } : OutputContainerProps) => {
  // Recommended:
  // {granularData &&...} => {Array.isArray(granularData) && granularData.map(...)}
  // Ensuring the granularData is ALWAYS an array
  return (
    <>
      <LiveOutput id="IcecatLive"></LiveOutput>
      {Array.isArray(granularData) && granularData.map(item => {
        return <GranularOutput id={item} key={item}></GranularOutput>
      })}
    </>
  )
}

type LiveContainerProps = {
  formData: FormDataProps
}

export const LiveContainer = ({ formData } : LiveContainerProps) => {
  const [show, setShow] = useState({
    popup: false,
    magic: false
  })
  const [error, setError] = useState("");

  // Cleanup of IcecatLive script
  // to prevent adding it multiple times
  // recommended by ChatGPT
  // Adding script on useEffect is simpler, but less interesting =)
  useEffect(()=>{
    return () => {
      const script = document.querySelector(`script[src="${ICECAT_LIVE_URL}"]`)
      if (script) script.remove()
    }
  })

  useEffect(() => {
    const resizeObserver = new ResizeObserver(() => {
      console.log('Resize observed');
    });
    const element = document.querySelector('#output-container');
    if (element) resizeObserver.observe(element);
  
    return () => {
      resizeObserver.disconnect();
    };
  }, []);
  

  if (formData.type !== "Live" || !getLinkLabel(formData)) return null

  const { language, granularData } = formData
  const divsArray = getDivs(granularData)
  const params = getParams(formData)

  const handleCloseLive = () => {
    setShow({ popup: false, magic: false })
  }

  const handleShowLive = () => {
    setShow(prev => ({ ...prev, magic: true }))
    const script = document.createElement('script');
    script.src = ICECAT_LIVE_URL
    script.async = true;
    document.body.appendChild(script);

    script.onload = () => {
      const live = window.IcecatLive
      if (!live) {
        setError("IcecatLive is not available. Please try again later.")
        return
      }

      live.getDatasheet(divsArray, params, language)
        .catch(error => {
          setError(error || "Failed to load product datasheet.")
        });

      setShow(prev =>({ ...prev, popup: true }))
    }
  }

  return (
    <>
      <Button buttonType="liveButton" text={getButtonText(show.magic)} onClick={handleShowLive} />
      {show.popup &&
        <LiveContainerAbsoluteStyles>
          <LiveContainerRelativeContainer>
            <StickyHeader>
              <CloseButton onClick={handleCloseLive}>❌</CloseButton>
            </StickyHeader>

            {!error ?
              <OutputContainer granularData={granularData} /> :
              <ErrorContainer error={error} />}

          </LiveContainerRelativeContainer>
        </LiveContainerAbsoluteStyles>}
    </>
  );
}