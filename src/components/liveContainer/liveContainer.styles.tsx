import styled, { keyframes } from "styled-components"

const flameColorChange = keyframes`
  0% {
    color: #ff9df0; /* Light pink */
  }
  25% {
    color: #cfa4ff; /* Reddish-pink */
  }
  50% {
    color: #ff8fc3; /* Bright red */
  }
  75% {
    color: #ffe481; /* Golden-yellow */
  }
  100% {
    color: #ff9df0; /* Loop back to light pink */
  }
`;

export const IcecatLiveContainer = styled.div`
  padding: 0em 1em;
`

export const LiveContainerAbsoluteStyles = styled.div`
  display: flex;
  position: absolute;
  z-index: 20;
  color: black;
  top: 0.75em;
  width: 75%;
  left: 12.5%;
  border-radius: 1.5em;
  box-shadow: 0 0 0.5em rgba(0, 0, 0, 0.5);
  background-color: #01011b;
  max-height: 90%;
  min-height: 700px;
  overflow: hidden;
  overflow-y: auto;
  overflow-x: hidden;
  `

export const LiveContainerRelativeContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  padding: 0em 1em 1em 1em; 
  width: 100%;
  height: 100%;
`

export const StickyHeader = styled.div`
  position: sticky;
  top: 0;
  display: flex;
  justify-content: end;
  background-color: #01011b;
  z-index: 25;
  `

export const CloseButton = styled.button`
  padding: 0.5em;
  background-color: transparent;
  border: none;
  transform: rotate(0deg);
  z-index: 99;
  cursor: pointer;
  
  &:hover {
    transition: rotate 0.5s;
    rotate: 180deg;
    text-shadow: 2px black;
  }
`

export const ErrorImage = styled.img`
  width: 100%;
  max-height: 50vh;
  object-fit: contain;
  opacity: 0.9;
`

export const Error = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  color: #b8d7ff;
  font-size: 1.6em;
  `
export const ErrorMessage = styled.span`
  color: #ff9df0;
  animation: ${flameColorChange} 5s ease-in-out infinite;
`

export const LiveOutput = styled.div`
padding: 0em 0.5em;
border-radius: 0.5em;
background: rgb(211,255,253);
background: linear-gradient(193deg, rgba(211,255,253,1) 0%, rgba(217,209,255,1) 43%, rgba(214,248,255,1) 100%); `

export const GranularOutput = styled(LiveOutput)`
  padding: 0em 0.5em;
  border-radius: 0em;
  background: rgb(211,255,253);
  background: linear-gradient(90deg, rgba(211,255,253,1) 0%, rgba(217,209,255,1) 43%, rgba(214,248,255,1) 100%); `
