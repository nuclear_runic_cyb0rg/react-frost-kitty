export const getDivs = (granularData : string[]) : any | any[] => {
    let filteredGranularData = granularData.filter(item => item !== "")
    if (filteredGranularData.length < 1) return '#IcecatLive' 
    return filteredGranularData.reduce<{}>((acc : any, nextItem : string) => {
      acc[nextItem] = `#${nextItem}`
      return acc
    }, {})
  }
  