import React, { ChangeEvent } from "react"
import { Column } from "../common/column.styles.tsx"
import { Row } from "../common/row.styles.tsx"
import { FormInput } from "../formInput/formInput.component.tsx"

const transformPlaceholder = (text : string) => {
  return `${text}1, ${text}2...`
}

type ProductIdentifiersProps = {
  type: string,
  isBatchRequested: boolean,
  handleInputChange: (event: ChangeEvent<HTMLInputElement>) => void
}

export const ProductIdentifiers = ({ type, isBatchRequested, handleInputChange } : ProductIdentifiersProps) => {
    const isFO = type === "FO"
    const showBrandAndMPN = !isFO && !isBatchRequested
    return (
      <Column>
        {showBrandAndMPN &&
          <Row>
            <FormInput data-input-for="mpn"
              onChange={handleInputChange}
              placeholder="MPN"
              style={{textTransform: "uppercase"}}
              />
            <FormInput data-input-for="brand"
              onChange={handleInputChange}
              placeholder="Brand" />
          </Row>}
  
        {!isFO &&
          <FormInput data-input-for="gtin"
            onChange={handleInputChange}
            placeholder={isBatchRequested ? transformPlaceholder("GTIN") : "GTIN"} />
          }
        <FormInput data-input-for="icecatID"
          onChange={handleInputChange}
          placeholder={isBatchRequested ? transformPlaceholder("IcecatID") : "IcecatID"} />
      </Column>)
  }