import React from "react"
import {
  ButtonStyles,
  TypeButtonStyles,
  ActiveButtonStyles,
  ActiveButtonShortStyles,
  ButtonLiveStyles
} from "./button.styles.tsx"

type ButtonProps = {
  buttonType?: string,
  text: string,
} & React.ButtonHTMLAttributes<HTMLButtonElement>

export const Button = ({ buttonType, text, ...props } : ButtonProps) => {
  if (!buttonType) {
    console.warn("Button type is required")
    return <ButtonStyles {...props}>{text}</ButtonStyles>
  }
  switch (buttonType) {
    case "typeButton":
      return (<TypeButtonStyles {...props}>{text}</TypeButtonStyles>)
    case "activeButton":
      return (<ActiveButtonStyles disabled={true} {...props}>{text}</ActiveButtonStyles>)
    case "activeButtonShort":
      return (<ActiveButtonShortStyles disabled={true} {...props}>{text}</ActiveButtonShortStyles>)
    case "liveButton":
      return (<ButtonLiveStyles {...props}>{text}</ButtonLiveStyles>)
    default:
      return (<ButtonStyles {...props}>{text}</ButtonStyles>)
  }
}