import styled from 'styled-components';

export const ButtonStyles = styled.button`
  background-color: #1e1e2f; /* Dark cold background */
  color: #c0e0f5; /* Light cold color for text */
  border: 2px solid #3a3a5c; /* Slightly darker border */
  border-radius: 8px;
  padding: 10px 20px;
  font-size: 1rem;
  font-weight: 500;
  cursor: pointer;
  transition: background-color 0.3s ease, box-shadow 0.3s ease;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);

  &:hover {
    background-color: #28334c; /* Darker cold color for hover */
    box-shadow: 0 6px 20px rgba(0, 0, 0, 0.2);
  }

  &:active {
    background-color: #3a4c69; /* Slight psychedelic hue for active state */
    box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
    transform: scale(0.98);
  }

  &:focus {
    outline: none;
    box-shadow: 0 0 0 3px rgba(67, 97, 238, 0.6); /* Psychedelic bluish hue for focus */
  }
`;

export const TypeButtonStyles = styled(ButtonStyles)`
  flex: 1;`


export const ActiveButtonShortStyles = styled(ButtonStyles)`
    background-color: #76e1ff;
    color: #00151a;
    box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);

    &:hover {
      background-color: #76e1ff;
      color: #00151a;
      box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
    }
`

export const ActiveButtonStyles = styled(ButtonStyles)`
    background-color: #05c9ff;
    color: #00151a;
    box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
    flex: 1;

    &:hover {
      background-color: #05c9ff;
      color: #00151a;
      box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
      flex: 1;
    }
`

export const ButtonLiveStyles = styled(ButtonStyles)`
  margin: 0.5em 2em;
  &:hover {
    /* background: linear-gradient(135deg, #1e1e2f, #5a5c9e, #9d4edd); */
    background: linear-gradient(135deg, #1e1e2f, #303060, #004930);


    color: #b4eaff;
  }
  `