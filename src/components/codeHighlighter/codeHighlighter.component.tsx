import React from 'react';
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter"
import  okaidia  from "react-syntax-highlighter/dist/esm/styles/prism/okaidia";

type CodeHighlighterProps = {
  content: string,
  language: string
}

export const CodeHighlighter = ({content, language} : CodeHighlighterProps) => {
  return (
    <SyntaxHighlighter language={language} style={okaidia}>
      {content}
    </SyntaxHighlighter>
  );
};
