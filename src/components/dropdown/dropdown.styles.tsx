import styled from 'styled-components';

export const DropdownStyles = styled.select`
    background-color: #1c1f26; /* Dark charcoal background */
    color: #b9feff;
    padding: 10px;
    border: 2px solid #4a5d7d; /* Subtle border color */
    border-radius: 15px;
    /* width: 50%; */

    &:hover {
        border: 2px solid #488bff; /* Subtle border color */
    }
`