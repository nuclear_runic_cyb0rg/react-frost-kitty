import React, { ChangeEvent } from "react";
import { DropdownsContainer } from "./conditionalInputs.styles.tsx";

import { FormCheckbox,
    FormInput,
    LanguageDropdown,
    DropdownStyles,
 } from "../"

type DropdownProps = {
  isXML: boolean;
  isCSV: boolean;
  handleOutputDropdownChange: (event : ChangeEvent<HTMLSelectElement>) => void;
};

const Dropdown = ({
  isXML,
  isCSV,
  handleOutputDropdownChange,
}: DropdownProps) => {
  if (!isXML && !isCSV) return null;
  return (
    <DropdownStyles onChange={handleOutputDropdownChange}>
      {isXML && (
        <>
          <option value="productxml">Regular XML</option>
          <option value="metaxml">Meta XML</option>
        </>
      )}
      {isCSV && (
        <>
          <option value="productcsv">Regular CSV</option>
          <option value="richproductcsv">Rich CSV</option>
        </>
      )}
    </DropdownStyles>
  );
};

type ConditionalInputsProps = {
  type: string;
  conditionalInputHandlers: { 
    handleOutputDropdownChange: (event: ChangeEvent<HTMLSelectElement>) => void,
    handleLanguageChange: (event: ChangeEvent<HTMLSelectElement>) => void,
    handleIsBatchRequested: (event: ChangeEvent<HTMLInputElement>) => void,
    handleIsWideFeatures: (event: ChangeEvent<HTMLInputElement>) => void,
    handleInputChange: (event: ChangeEvent<HTMLInputElement>) => void
    handlePSVersionChange: (event: ChangeEvent<HTMLInputElement>) => void
   };
  activeLang: string;
};

export const ConditionalInputs = ({
  type,
  conditionalInputHandlers,
  activeLang,
}: ConditionalInputsProps) => {
  const checks = {
    isXML: type === "XML",
    isLive: type === "Live",
    isCSV: type === "CSV",
    isFO: type === "FO",
    isJSON: type === "JSON",
  };

  const {
    handleOutputDropdownChange,
    handleLanguageChange,
    handleIsBatchRequested,
    handleIsWideFeatures,
    handleInputChange,
    handlePSVersionChange,
  } = conditionalInputHandlers;
  const isBatchLblTxt = "I have comma-separated GTINs or IDs";
  const isBatchLblValue = "isBatchRequested";

  const isWideLblTxt = "Use feature names as headers";
  const isWideLblValue = "isWideFeatures";

  const isProductStoryv2LabelTxt = "Use Product Story v2";
  const isProductStoryv2LabelValue = "productStoryVersion";

  return (
    <DropdownsContainer>
      {!checks.isLive && (
        <FormCheckbox
          checkboxName={isBatchLblValue}
          labelValue={isBatchLblTxt}
          onCheck={handleIsBatchRequested}
        />
      )}
      
      {checks.isLive && (
        <FormCheckbox
          checkboxName={isProductStoryv2LabelValue}
          labelValue={isProductStoryv2LabelTxt}
          onCheck={handlePSVersionChange}
        />
      )}

      {!checks.isFO && (
        <LanguageDropdown
          key={type}
          activeValue={activeLang}
          handleLanguageChange={handleLanguageChange}
          includeINT={checks.isXML}
          sortVertical={false}
        />
      )}

      {checks.isCSV && (
        <FormCheckbox
          checkboxName={isWideLblValue}
          labelValue={isWideLblTxt}
          onCheck={handleIsWideFeatures}
        />
      )}

      {(checks.isXML || checks.isJSON) && (
        <FormInput
          data-input-for="relationsLimit"
          onChange={handleInputChange}
          placeholder="Relations limit (max 3000)"
        />
      )}

      <Dropdown
        isXML={checks.isXML}
        isCSV={checks.isCSV}
        handleOutputDropdownChange={handleOutputDropdownChange}
      />

    </DropdownsContainer>
  );
};
