import styled from "styled-components"


export const DropdownsContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(2, minmax(0, 1fr));
    gap: 10px;
    padding: 0px 3rem;
    @media (max-width: 1000px) {
        padding: 0px 1rem;
    }
`