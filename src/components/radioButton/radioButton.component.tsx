import React, { ChangeEvent } from "react";
import { Label } from "./radioButton.styles.tsx";

type RadioButtonProps = {
  value: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  checked: boolean;
  name: string;
};

export const RadioButton = ({
  value,
  checked,
  name,
  ...props
}: RadioButtonProps) => {
  return (
    <Label>
      <input
        type="radio"
        name={name}
        value={value}
        checked={checked}
        {...props}
      />
      {value.toString().toUpperCase()}
    </Label>
  );
};
