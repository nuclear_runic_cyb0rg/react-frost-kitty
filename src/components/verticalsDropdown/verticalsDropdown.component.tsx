import React, { ChangeEvent } from "react";
import { DropdownStyles } from "../dropdown/dropdown.styles.tsx";

type VerticalsDropdownProps = {
  onChange: (event : ChangeEvent<HTMLSelectElement>) => void;
};

export const verticalsArray = [
  { id: 107, label: "Telecom & Navigation" },
  { id: 300, label: "Office" },
  { id: 1058, label: "AV & Photo" },
  { id: 1265, label: "Food, Beverages & Tobacco" },
  { id: 1280, label: "Health, Beauty & Personal Care" },
  { id: 1314, label: "Domestic Appliances" },
  { id: 1575, label: "Fashion & Lifestyle" },
  { id: 1960, label: "Sports & Recreation" },
  { id: 1965, label: "Vehicles" },
  { id: 1980, label: "Baby & Children" },
  { id: 2248, label: "Pet Care" },
  { id: 2295, label: "Toys" },
  { id: 2332, label: "Lighting" },
  { id: 2830, label: "Home" },
  { id: 2833, label: "Computers & Peripherals" },
  { id: 2835, label: "Industrial & Lab Equipment" },
  { id: 2836, label: "Entertainment & Hobby" },
  { id: 3877, label: "Online Data Services" },
  { id: 4776, label: "Building & Construction" },
  { id: 4860, label: "Drugs & Pharmaceutics" },
  { id: 5233, label: "Hospitality" },
  { id: 5401, label: "Medical Equipment" },
  { id: 7775, label: "Defense, Police & Safety" }
]

export const VerticalsDropdown = ({
  onChange,
}: VerticalsDropdownProps) => {
  return (
    <DropdownStyles onChange={onChange}>
      {verticalsArray.map((vertical) => {
        return (
          <option key={vertical.id} value={vertical.id}>
            {vertical.label}
          </option>
        );
      })}
    </DropdownStyles>
  );
};
