import { DropdownStyles } from "../"
import { useLocalesContext } from "../../contexts"
import React, { ChangeEvent } from "react"



const addOption = (language : LocaleItem) => {
    const { value, label, country } = language
    return (
        <option key={value} value={value}>
            {label} {country ? `(${country})` : ""}
        </option>)
}

type LanguageDropdownProps = {
    handleLanguageChange: (event : ChangeEvent<HTMLSelectElement>) => void,
    includeINT: boolean,
    sortVertical: boolean,
    activeValue: string
}

export const LanguageDropdown = ({
    handleLanguageChange,
    includeINT,
    sortVertical = false,
    activeValue = "EN" } : LanguageDropdownProps) => {
    const { locales, filterVerticals } = useLocalesContext()
    const verticalLanguagesArray = filterVerticals(locales)

    const activeArray = sortVertical ? verticalLanguagesArray : locales

    const includeInternational = () => {
        return includeINT && <option key="INT" value="INT">🌏 International</option>
    }

    return (<>
        <DropdownStyles id="form-data-language" onChange={handleLanguageChange} value={ activeValue }>
            {includeInternational()}

                { activeArray.map((languageGroup) => {
                const { group, items } = languageGroup as LocaleGroup
                if (!items) return addOption(languageGroup as LocaleItem)
                return (
                    <optgroup label={group} key={group}>
                        {items.map(item => addOption(item))}
                    </optgroup>)
            })}
        </DropdownStyles>

    </>)
}