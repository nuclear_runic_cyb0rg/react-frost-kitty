import React, { ChangeEvent } from 'react'
import { Column } from "../common/column.styles.tsx"
import { FormCheckbox } from "../formCheckbox/formCheckbox.component.tsx"
import { GranularGrid } from "./granularMenu.styles.tsx"

const granularArray = [
  { value: 'essentialinfo', label: 'Essential Info' },
  { value: 'gallery', label: 'Gallery' },
  { value: 'title', label: 'Title' },
  { value: 'marketingtext', label: 'Marketing Text' },
  { value: 'multimedia', label: 'Multimedia' },
  { value: 'videos', label: 'Videos' },
  { value: 'productstory', label: 'Product Story' },
  { value: 'tours3d', label: 'Tours 3D' },
  { value: 'reasonstobuy', label: 'Reasons to Buy' },
  { value: 'featuregroups', label: 'Features' },
  { value: 'featurelogos', label: 'Feature Logos' },
  { value: 'manuals', label: 'Manuals' },
  { value: 'reviews', label: 'Reviews' },
]

type GranularMenuProps = {
  type: string,
  handleActivateCheckbox: (event: ChangeEvent<HTMLInputElement>) => void
}

export const GranularMenu = ({ type, handleActivateCheckbox} : GranularMenuProps) => {
    if (type !== "JSON" && type !== "Live") return null
    return (<Column>
      <legend>Granular data</legend>
      <GranularGrid>
        {granularArray.map(item => {
          return (<FormCheckbox
            key={`${item.label}`}
            id={"granular-" + item.value}
            checkboxName={item.value}
            labelValue={item.label}
            onCheck={handleActivateCheckbox} />)
        })}
      </GranularGrid>
    </Column>)
  }