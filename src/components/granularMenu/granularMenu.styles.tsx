import styled from "styled-components"

export const GranularGrid = styled.div`
  padding: 0px 5rem;
  display: grid;
  grid-template-columns: repeat(4, auto);
  @media (max-width: 1200px) {
    grid-template-columns: repeat(3, auto);
    padding: 0px 3rem;
  }

  @media (max-width: 900px) {
    grid-template-columns: repeat(2, auto);
    padding: 0px 2rem;
  }

  @media (max-width: 600px) {
    grid-template-columns: repeat(1, auto);
    padding: 0px 1rem;
  }
`
