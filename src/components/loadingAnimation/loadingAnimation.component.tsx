import React from "react";
import styled, { keyframes } from "styled-components";


const LoadingAnimationStyles = styled.div`
  position: relative;
  z-index: 10;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const slightlyJump = keyframes`
  0% {
    transform: translateY(0);
  }

  50% {
    transform: translateY(-5px);
  }
  100% {
    transform: translateY(0);
  }
`;



const Title = styled.div`
  animation: ${slightlyJump} 1s ease-in-out infinite alternate;
`;

export const LoadingAnimation = () => {
  return (
    <LoadingAnimationStyles>
      <Title> ⏳ Loading...</Title>
    </LoadingAnimationStyles>
  );
};
