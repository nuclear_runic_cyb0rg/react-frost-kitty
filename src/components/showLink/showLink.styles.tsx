import styled from "styled-components"

export const FinalLink = styled.a`
  color: #97a3ff;
  margin: 1em 1em 0em 1em;
  text-decoration: none;
  text-align: center;
  font-weight: bold;
  transition: all 0.3s ease-in-out;
  padding: 5px;
  border-radius: 5px;
  border: 2px solid #97a3ff;
  &:hover {
    background: rgb(123,246,255);
    /* background: linear-gradient(90deg, rgba(123,246,255,1) 0%, rgba(120,255,106,1) 48%, rgba(238,168,242,1) 100%);  */
    text-decoration: underline;
    color: #250042;
    
  }

  &:visited {
    color: #7f4a79;
    border-color: #7f4a79;
  }
`