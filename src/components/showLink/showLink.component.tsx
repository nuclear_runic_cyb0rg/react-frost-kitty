import React from "react"
import { FinalLink } from "./showLink.styles.tsx"

type ShowLinkProps = {
    requestType: string,
    link: string,
    matchedBy: string
}
export const ShowLink = ({ requestType, link, matchedBy } : ShowLinkProps) => {
    
    const label = `${requestType} LINK for ${matchedBy}`
    return (
      <FinalLink href={link}
        target="_blank"
        rel="noreferrer">
        {label}
      </FinalLink>)
  }