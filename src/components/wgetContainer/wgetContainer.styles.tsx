import styled from "styled-components"

export const RequestContainer = styled.div`
    background-color: #0c0130;
    color: #b9ffff;
    padding: 14px;    
    white-space: pre;
    overflow: hidden;
    cursor: pointer;
    border-radius: 8px;
    transition: background-color 0.3s ease, transform 0.2s ease; // Smooth transitions for the effects
    
    &:hover {
        background-color: #220a63; // Change background color on hover
        transform: scale(1.01); // Slightly scale up on hover
    }

    &:active {
        background-color: #003308; // Change background color on click
        transform: scale(0.98); // Slightly scale down on click
    }
`;

export const Overlay = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(1, 52, 3, 0.8);
    color: #c1f0ff;
    font-weight: bold;
    display: flex;
    justify-content: center;
    align-items: center;
`