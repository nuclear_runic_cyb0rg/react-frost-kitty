import React, { useState } from "react";
import { RequestContainer, Overlay } from "./wgetContainer.styles.tsx";
import { createWgetFromLink } from "../../helpers/index.ts";
import { useUserContext, useTokensContext } from "../../contexts/index.ts";

//* RETURNS Copy-able clickable text container (for shell commands)
//TODO Currently directtly uses contexts, need to abstract this motherfucker
//? Or maybe not to fix what is working

type WgetContainerProps = {
  link: string;
};
export const WgetContainer = ({ link }: WgetContainerProps) => {
  const [showCopied, setShowCopied] = useState(false);

  const { user } = useUserContext();
  const { tokens } = useTokensContext();
  const { api_token, content_token } = tokens;
  const isTokensEnabled = api_token || content_token;
  const timeoutInMs = 500;
  const popUpOnCopyText = "Copied!";

  const writeToClipboard = (text: string) => {
    navigator.clipboard.writeText(text);
  };

  const getWgetText = () => {
    return isTokensEnabled
      ? createWgetFromLink(link, user, api_token, content_token)
      : createWgetFromLink(link, user);
  };
  const handleCopyWget = () => {
    writeToClipboard(getWgetText());
    setShowCopied(true);
    setTimeout(() => {
      setShowCopied(false);
    }, timeoutInMs);
  };

  return (
    <RequestContainer onClick={handleCopyWget}>
      {getWgetText()}
      {showCopied && <Overlay>{popUpOnCopyText}</Overlay>}
    </RequestContainer>
  );
};
