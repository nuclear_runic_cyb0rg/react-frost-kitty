import styled from 'styled-components'

export const FormInputStyles = styled.input`
        background-color: #1c1f26; /* Dark charcoal background */
        border: 2px solid #4a5d7d; /* Subtle gradient-like border */
        color: #d1e3f2; /* Icy blue text color */
        padding: 10px;
        border-radius: 8px; /* Slightly rounded corners for a playful touch */
        font-family: 'Poppins', sans-serif; /* Modern and slightly playful font */
        box-shadow: 0 4px 6px rgba(0, 0, 0, 0.4); /* Soft shadow */
        transition: border-color 0.3s ease, box-shadow 0.3s ease;
    &:focus {
        border-color: #5e76a5; /* Slightly vibrant border on focus */
        box-shadow: 0 4px 12px rgba(94, 118, 165, 0.6); /* Enhanced shadow on focus */
        outline: none; /* Remove default outline */
}
`

export const FormInputError = styled(FormInputStyles)`
    color: red;
    border-color: red;
    `

export const FormInputCheck = styled(FormInputStyles)`
    color: green;
    border-color: green;
    `