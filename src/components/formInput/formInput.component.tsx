import React from 'react'
import { FormInputStyles } from './formInput.styles.tsx'
export const FormInput = (props : { [key: string]: any }) => {
    return (<FormInputStyles {...props} />)
}