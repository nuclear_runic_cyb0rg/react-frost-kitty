import { ConfigProps } from "./constructProductLink.helper"

const REQUEST_TYPES_CONFIG : ConfigProps = {
    JSON: {
        domain: "https://live.icecat.biz/api?",
        separator: "=",
        paramTitles: {
            username: "shopname",
            language: "lang",
            appKey: "app_key",

            brand: "Brand",
            mpn: "ProductCode",
            gtin: "GTIN",
            icecatID: "icecat_id",

            granularData: "content",
            relationsLimit: "relationsLimit",

        }
    },
    XML: {
        domain: "https://data.icecat.biz/xml_s3/xml_server3.cgi?",
        separator: "=",
        paramTitles: {
            language: "lang",
            brand: "vendor",
            mpn: "prod_id",
            gtin: "ean_upc",
            icecatID: "icecat_id",

            output: "output",
            relationsLimit: "relations_limit"
        }
    },
    CSV: {
        domain: "https://data.icecat.biz/xml_s3/xml_server3.cgi?",
        separator: "=",
        paramTitles: {
            language: "lang",
            brand: "vendor",
            mpn: "prod_id",
            gtin: "ean_upc",
            icecatID: "icecat_id",
            output: "output",
            isWideFeatures: "wide_features_format"
        }
    }

}

// REQUEST_TYPES_CONFIG.CSV = {
//     ...REQUEST_TYPES_CONFIG.XML,
//     paramTitles: {
//         ...REQUEST_TYPES_CONFIG.XML.paramTitles,
//         isWideFeatures: "wide_features_format"
//     }
// }

export { REQUEST_TYPES_CONFIG };