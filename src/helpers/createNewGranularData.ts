export const createNewGranularData = (checked : boolean, value : string, formData : FormDataProps) => {
let newGranularDataArray = [...formData.granularData]
    // If checked, just add a value to a return object
    if (checked) newGranularDataArray.push(value)
    // If not, filter list to delete the unselected option
    else {
      newGranularDataArray = newGranularDataArray.filter(item => item !== value)
    }

    // Remove duplicates and filter out empty strings
    return [...new Set(newGranularDataArray)].filter(item => item !== "")
}