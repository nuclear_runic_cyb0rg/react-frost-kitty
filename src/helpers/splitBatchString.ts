export const splitBatchString = (str : string) : string[] => {
    return str
      .split(',')
      .filter(identifier => identifier)
      .map(identifier => identifier.trim())
  }