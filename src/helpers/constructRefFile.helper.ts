

export const constructRefFileLink = (file: string, isFull: boolean, isGzipped: boolean, format : string, isUTF8 = false) => { 
    let getFolder = () => {
        const isXML = format === "xml"
        
        let folder = isFull ? "level4" : "freexml"

        if (!isFull && !isXML) folder += '.int'
        
        return folder + (isXML ? "/refs" : "/csv")
        

    } 
    const addGzForGzipped = isGzipped ? ".gz" : ""
    const addUTF8 = isUTF8 ? ".utf8" : ""
    return `https://data.icecat.biz/export/${getFolder()}/${file}.${format}${addUTF8}${addGzForGzipped}`

}