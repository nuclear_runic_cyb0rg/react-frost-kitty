type ConstructIndexFileLinkProps = {
  file: string;
  isFull: boolean;
  language: string;
  verticalID: string;
  format: string;
};

export const constructIndexFileLink = ({
  file,
  isFull,
  language,
  verticalID,
  format,
}: ConstructIndexFileLinkProps) : string => {
  const domain = "https://data.icecat.biz/export/";
  const subFolder = isFull ? "level4" : "freexml";

  let url = `${domain}${subFolder}/${language}/`;

  if (verticalID && file === "files.index") {
    url += `${verticalID}.vertical.`;
  }
  url += `${file}.${format}.gz`;

  return url;
};
