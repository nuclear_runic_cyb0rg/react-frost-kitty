import { constructFOLink } from "./constructFOLink.helper.ts";
import { constructProductLink } from "./constructProductLink.helper.ts";

export const getLinksArrayFromBatch = (
  formData: FormDataProps,
  identifiersArray: string[],
  key: string
): BatchLinksArrayProps => {

  const isFO = formData.type === "FO";
  const newBatchLinksArray : BatchLinksArrayProps = [];

  const labels: any = {
    gtin: "GTIN",
    icecatID: "ID",
  };
  const label: string = labels[key];

  for (let id of identifiersArray) {
    const link: string = isFO
      ? constructFOLink(id)
      : constructProductLink({ ...formData, [key]: id });
    const matchedBy = `${label} ${id}`;
    newBatchLinksArray.push({ link, matchedBy });
  }
  return newBatchLinksArray
};
