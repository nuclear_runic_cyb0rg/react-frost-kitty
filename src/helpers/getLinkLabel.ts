export const getLinkLabel = ({icecatID, gtin, brand, mpn} : FormDataProps) : string => {
    // If ICECAT_ID return IcecatID
    // if GTIN return gtin
    // if BRAND and MPN return 'MPN/BRAND'
    // else return empty string
    return icecatID || gtin || (brand && mpn ? `${mpn}/${brand}` : '');
  };