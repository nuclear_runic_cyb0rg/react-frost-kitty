export { constructFOLink } from './constructFOLink.helper';
export { constructIndexFileLink} from './constructIndexFileLink'
export { constructRefFileLink } from './constructRefFile.helper'
export { constructProductLink } from './constructProductLink.helper'
export { createWgetFromLink } from './createRequestFromLink'
export { getLinksArrayFromBatch } from "./getLinksArrayFromBatch";
export { getOutputForNewType } from "./getOutputForNewType";
export { createBatchLinksArray } from "./createBatchLinksArray"; 
export { createNewGranularData } from "./createNewGranularData";
export { splitBatchString } from "./splitBatchString";

