import { splitBatchString } from "./";
import { getLinksArrayFromBatch } from "./index";

export const createBatchLinksArray = (
  gtinString: string,
  idString: string,
  formData: FormDataProps
): BatchLinksArrayProps => {
  const gtinBatchArray = splitBatchString(gtinString);
  const icecatIDBatchArray = splitBatchString(idString);

  const finalArray = [
    ...(formData.type !== "FO"
      ? getLinksArrayFromBatch(formData, gtinBatchArray, "gtin")
      : []),
    ...getLinksArrayFromBatch(formData, icecatIDBatchArray, "icecatID"),
  ].filter((obj) => {
    return Object.keys(obj).length > 0;
  });

  return finalArray as BatchLinksArrayProps;
};
