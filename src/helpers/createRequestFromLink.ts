// wget --username=<USER> --ask-password --continue \
//   --header 'api-token: 2a473644-949e-4df4-af3f-7249da950e4d' \
//   --header 'content-token: 1234567890-234567-asdfghj' \
//   --output-document \
//   - 'https://live.icecat.biz/api?some=param&another=value&final=value'

export const createWgetFromLink = (
    link : string,
    user : string, 
    accessToken='', 
    contentToken='') => {
    let wgetRequest = `wget --http-user=${user} --ask-password --continue \\\n`
    if (accessToken) wgetRequest += `--header 'api-token: ${accessToken}' \\\n`
    if (contentToken) wgetRequest += `--header 'content-token: ${contentToken}' \\\n`

    // console.log(link)
    
    return wgetRequest.concat(link)
}
