import { REQUEST_TYPES_CONFIG } from "./constructProductLink.config.ts"


const _removeEmptyValues = (formData : FormDataProps) =>{
    const newObject = Object.fromEntries(
        Object.entries(formData)
            .filter(([key, value]) => Boolean(value))
    )
    return newObject
} 

export type ConfigProp = {
    domain: string,
    separator: string,
    paramTitles: {
        [key: string]: string | undefined
    }
}

export type ConfigProps = {
    [key: string]: ConfigProp
}

export const constructProductLink = (formData : FormDataProps) : string => {
    // Live request is not a link!
    if (formData.type === "Live") return ''
    // Immediately filter empty values
    let userInputData = _removeEmptyValues(formData)

    const type = userInputData.type.toString().toUpperCase()
    
    const {
        domain,
        separator: equals,
        paramTitles
    }: ConfigProp = REQUEST_TYPES_CONFIG[type];

    const keys = Object.keys(userInputData).filter(key => key !== "type")
    const url = keys.reduce((acc, nextKey) => {
        // Immediately return if the key is not supported
        if (!(nextKey in paramTitles)) return acc

        nextKey = nextKey.toString().trim()

        const param = paramTitles[nextKey]
     
        const value = encodeURIComponent(userInputData[nextKey].toString()) 
        return acc += `${param}${equals}${value}&`
    }, domain)
    return url.slice(0, -1)
}