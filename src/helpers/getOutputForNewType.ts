export const getOutputForNewType = (value : string) : string => {
    if (value === "CSV" || value === "XML") return `product${value.toLowerCase()}`
    return ""
  }