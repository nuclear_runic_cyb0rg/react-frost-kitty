const DOMAIN = 'https://cliff-racer-db.netlify.app/.netlify/functions/api?collection='

export const fetchCollectionAsync = async (collectionName : string) : Promise<any> => {
    const URL = DOMAIN + collectionName
    // console.log(URL)
    try {
        let response = await fetch(URL)
        let text = await response.text()
        // console.log(text)
        return JSON.parse(text)
    }
    catch (error) {
        console.log(error)
        return 0
    }
}