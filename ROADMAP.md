# ROADMAP

## Project Goal

A comprehencive Icecat Helper serves as a tool to help users find and access the Icecat API. Completes three tasks:

+ Icecat Fetcher for all API types
+ Export files (Index and Refs) with explanations, links and code to fetch safely
+ Manuals in one place, including my personal notes

## Milestones

+ version 0.1.0 - Home component: design and replicate JSON
+ version 0.1.1 - Home component: add XML_s3 request and corresponding fields
+ version 0.1.3 - Home component: stylize the API UI with UX and other stuff
+ version 0.1.4 - Navigation component: create and add links to Home and Index files
+ version 0.1.5 - Index component: initialize and design index component
+ version 1.0.0 - alpha release, includes Ref Files, Index Files and GetProduct (except Live)
+ version 1.0.1 - beta release, includes Live and UI responsiveness, also deployment!
+ add supplementary MongoDB to store hard data (cliff-racer-db.netlify.app)
(WE ARE HERE!)
+ version 1.0.4 - add Manuals (fetch from collection in MongoDB)

## Considerations

+ TypeScript
+ User Authentication and Roles (for 2 fields?)
+ Data Validation (don't we need have an option to test the API?)

## UI/UX Guidelines

Design choices are my personal preferences as I want to have fun creating it.

+ Color scheme: dark-orienter, cold colors, with little psychedelic hues
+ Design: a bit playful, but overall modern and clean
+ Layout: centered, flex where possible
+ Components: Button (two types), ImageButton, FormInput, Navigation, CodeHighlighter

## TODOs

+ Home:  
  + ✅RequestTypeMenu
  + ✅Credentials
  + ✅ProductIdentifiers
  + ✅ConditionalInputs
  + ✅GranularData
  + ✅TokensInput
  + ✅LiveContainer
  + ✅Output
  + Request types:
      ✅Get XML data from s3 storage
      ✅Get CSV data from s3 storage
      📍XML_by_ID
      ✅Live request that returns JSON
      📍CSV_by_ID
      ✅Live request that returns HTML
      ✅icecat.biz link to the product (Demo)
+ Index files:
  + ✅UI for quick access to Index files
  + ✅Index files with expandable explanations
  + Search functionality
  + ✅Add Schema
  + ✅Add Schema for CSV
+ Reference files:
  + ✅Same as Index files
+ Manuals:
  + 📍Complete UI
