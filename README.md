# Main Info

Helper for Icecat APIs. Hosted on [Netlify](https://frost-kitty.netlify.app/). Development is going on on the `dev` branch.

Includes:

+ UI for main requests in Icecat: XML, CSV, JSON
+ Index Files: all types, schemas, info and wGet builder
+ Reference Files: all types, schemas, info and wGet builder
+ Renders Live requests in separate component

## Available Scripts

In the root directory run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

For minified web-compatible version, run:

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

## Versioning

Currently version is symbolic, mostly showing advancement in Roadmap.

## Technologies

+ React
+ Icecat Live API
+ node and MongoDB to store data
+ Styled Components
+ Netlify

## Roadmap

Check ROADMAP.md for more details.
